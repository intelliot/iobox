FROM python:3.9-bullseye

WORKDIR /app
COPY . .
RUN pip install .

EXPOSE 5000

CMD ["iobox", "/app/config.ini"]
