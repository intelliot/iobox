import threading
from flask import Flask

app = Flask("iobox")


def start(routes, conf):
    for url, verbs in routes.items():
        methods=[]
        for verb, handler in verbs.items():
            methods.append(str(verb))
            app.add_url_rule(
                rule=url,
                endpoint = f"{url}_{verb}",
                view_func = handler,
                methods = methods
            ) 

    threading.Thread(target=lambda: app.run(
        host=conf["api"]["host"],
        port=conf["api"]["port"],
        debug=(conf["app"]["debug"] == "true"),
        use_reloader=False),
    name = "iobox api").start()
