#!/usr/bin/env python3
import signal
import logging
import argparse
from iobox.fake import lamp
from iobox.iface import interface
from iobox.device import device
from iobox.api import api
from . import config


def handler(signum, frame):
    signame = signal.Signals(signum).name
    print(f'Signal handler called with signal {signame} ({signum})')
    exit(0)


def start():
    logging.basicConfig(level = logging.INFO)

    signal.signal(signal.SIGINT, handler)
    signal.signal(signal.SIGQUIT, handler)
    signal.signal(signal.SIGTERM, handler)

    parser = argparse.ArgumentParser(prog='iobox')
    parser.add_argument('filename')
    args = parser.parse_args()

    conf = config.read(args.filename)
    lamp.LampDevice("fakelamp1", conf)
    lamp.LampDevice("fakelamp2", conf)

    logging.info('Starting with configuration: %s', args.filename)
    logging.info('Supported interfaces: %s', interface.SUPPORTED)

    ih = interface.Handler(conf)
    dh = device.DeviceHandler(conf, ih)

    api.start(dh.routes(), conf)
