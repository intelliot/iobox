import configparser

# Read, reads the configuration file.
# It returns a dictionary with the key/value pairs.
#
# Configuration file is a standard ini file.
# The main sections are:
#
#   - app, general iobox configuration
#   - api, api related configuration
#
# After those, it has one section per device installed.
#
def read(filename):
    confd = dict()
    parser = configparser.ConfigParser()
    parser.read(filename)

    for section in parser.sections():
        confd[section] = dict()
        
        for option in parser.options(section):
            confd[section][option] = parser.get(section, option)

    return confd
