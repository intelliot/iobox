import threading
import time
import queue
import json
import struct
from flask import jsonify

class LM75b():
    def __init__(self, name, options) -> None:
        self.name = name
        self.options = options
        self.readq = queue.Queue(maxsize=100)

        # Resources consists of the actual resource value and:
        #
        # - TS  : last measurement timestamp (in unix epoch) 
        #         When this is set to 0, no value has been
        #         received yet
        # - Lock: for safe access

        # Temperature resource
        self.temperature = 0
        self.temperatureTS = 0
        self.temperatureLock = threading.Lock()


    def run(self):
        while True:
            msg = self.readq.get()
            ts = int(time.time())

            self.decodeTemperature(msg, ts)
            self.readq.task_done()


    def decodeTemperature(self, msg, ts):
        raw = ((msg << 8) & 0xFF00) + (msg >> 8)
        val = (raw / 32.0) / 8.0

        self.temperatureLock.acquire()
        self.temperature = val
        self.temperatureTS = ts
        self.temperatureLock.release()



    def getTemperature(self):
        self.temperatureLock.acquire()
        ts = self.temperatureTS
        val = self.temperature
        self.temperatureLock.release()

        if ts == 0:
            return jsonify("{}")

        tdict = dict(temperature = val, timestamp = ts)

        return jsonify(tdict)

    def rwq(self):
        return [self.readq]

    def routes(self):
        routes=dict()
        routes["/devices/i2c/" + self.name + "/temperature"] = dict(GET=self.getTemperature)
        return routes
