import threading
import time
import queue
import json
from flask import jsonify
from flask import request
import logging


class Fakelamp():
    def __init__(self, name, options) -> None:
        self.name = name
        self.options = options
        self.readq = queue.Queue(maxsize=100)
        self.writeq = queue.Queue(maxsize=100)
        self.statusq = queue.Queue(maxsize=100)

        # Resources consists of the actual resource value and:
        #
        # - TS  : last measurement timestamp (in unix epoch) 
        #         When this is set to 0, no value has been
        #         received yet
        # - Lock: for safe access

        # Status resource
        self.status = 0
        self.statusTS = 0
        self.statusLock = threading.Lock()


    def run(self):
        while True:
            msg = self.readq.get()
            logging.info("serial handler received msg: ", msg)
            self.readq.task_done()

            self.statusLock.acquire()
            try:
                self.status = int(msg) % 2
                tdict = dict(
                    status = self.status,
                    timestamp = self.statusTS
                )
            except:
                tdict = dict(error = "bad input")
            self.statusLock.release()

            self.statusq.put(tdict)


    def getStatus(self):
        self.writeq.put("GET")
        answer = self.statusq.get()
        self.statusq.task_done()

        return jsonify(answer)


    def setStatus(self):
        status = request.json["status"]

        if status == 1 or status == 0:
            self.statusLock.acquire()
            self.status = status
            self.statusTS = int(time.time())
            self.statusLock.release()

            self.writeq.put("SET "+str(status))

        return self.getStatus()


    def rwq(self):
        return [self.readq, self.writeq]


    def routes(self):
        routes = dict()
        routes["/devices/serial/" + self.name + "/status"] = dict(GET=self.getStatus, PUT=self.setStatus)
        return routes
