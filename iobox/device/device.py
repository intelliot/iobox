import logging
import threading
import queue
from flask import jsonify
from . import dev_mirocard as devMirocard 
from . import dev_fakelamp as devFakelamp
from . import dev_lm75b as devLM75b

SUPPORTED = ["mirocard", "fakelamp", "lm75b"]


class DeviceHandler():
    def __init__(self, conf, ih) -> None:
        self.ih = ih
        self.devQueue = dict()
        self.devices = []
        self.conf = conf
        self.i2cDevices = []
        self.serialDevices = []
        self.bleDevices = []

        for section in conf:
            for option in conf[section]:
                if option == "type" and conf[section][option] in SUPPORTED:
                    self.startDevice(section, conf[section])
        
        ih.start()

        for d in self.devices:
            threading.Thread(target=d.run, name=d.name+" runner").start()


    def startDevice(self, name, options):
        logging.info("Starting device %s with options: %s", name, options)

        if options["type"] == "mirocard":
            self.startMirocardHandler(name, options)
        elif options["type"] == "fakelamp":
            self.startFakelampHandler(name, options)
        elif options["type"] == "lm75b":
            self.startLM75bHandler(name, options)
        else:
            logging.info("Skip unknown device: %s", name)


    def startMirocardHandler(self, name, options):
        # Create mirocard queue
        self.devQueue = queue.Queue(maxsize=100)

        # Create mirocard instance
        self.devices.append(devMirocard.Mirocard(name, options, self.devQueue))

        # Register mirocard instance to the appropriate interface
        self.ih.registerModule("ble", options["mac"], self.devQueue)
        self.bleDevices.append(self.conf[name])


    def startFakelampHandler(self, name, options):
        # Create serial device instance
        self.devices.append(devFakelamp.Fakelamp(name, options))
        devCtx = self.devices[-1].rwq()

        # Register serial device instance to the appropriate interface
        self.ih.registerModule("serial", options["port"], devCtx)
        self.serialDevices.append(self.conf[name])


    def startLM75bHandler(self, name, options):
        # Create i2c device instance
        self.devices.append(devLM75b.LM75b(name, options))

        devCtx = dict()
        devCtx["bus"] = options["bus"]
        devCtx["address"] = options["address"]
        devCtx["register"] = options["register"]
        devCtx["rwq"] = self.devices[-1].rwq()

        # Register i2c device instance to the appropriate interface
        self.ih.registerModule("i2c", name, devCtx)
        self.i2cDevices.append(self.conf[name])


    def routes(self):
        allRoutes = dict()

        allRoutes["/"] = dict(GET=self.getHealth)
        allRoutes["/devices"] = dict(GET=self.getDevices)

        if len(self.bleDevices) > 0:
            allRoutes["/devices/ble"] = dict(GET=self.getBleDevices)
        
        if len(self.serialDevices) > 0:
            allRoutes["/devices/serial"] = dict(GET=self.getSerialDevices)

        if len(self.i2cDevices) > 0:
            allRoutes["/devices/i2c"] = dict(GET=self.getI2CDevices)

        for d in self.devices:
            routes = d.routes()
            for url in routes:
                allRoutes[url] = routes[url]
    
        return allRoutes


    def getHealth(self):
        return jsonify(dict())


    def getDevices(self):
        return jsonify(
            dict(ble=len(self.bleDevices),
                 serial=len(self.serialDevices),
                 i2c=len(self.i2cDevices)
            )
        )


    def getBleDevices(self):
        result = dict()

        for d in self.devices:
            if d.options["module"] == "ble":
                result[d.name] = d.options

        return jsonify(result)


    def getSerialDevices(self):
        result = dict()

        for d in self.devices:
            if d.options["module"] == "serial":
                result[d.name] = d.options

        return jsonify(result)

    def getI2CDevices(self):
        result = dict()

        for d in self.devices:
            if d.options["module"] == "i2c":
                result[d.name] = d.options

        return jsonify(result)
