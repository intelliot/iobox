import threading
import time
import struct
from flask import jsonify


class Mirocard():
    def __init__(self, name, options, queue) -> None:
        self.name = name
        self.options = options
        self.queue = queue

        # Resources consists of the actual resource value and:
        #
        # - TS  : last measurement timestamp (in unix epoch) 
        #         When this is set to 0, no value has been
        #         received yet
        # - Lock: for safe access

        # Temperature resource
        self.temperature = 0
        self.temperatureTS = 0
        self.temperatureLock = threading.Lock()

        # Humidity resource
        self.humidity = 0
        self.humidityTS = 0
        self.humidityLock = threading.Lock()


    def run(self):
        while True:
            msg = self.queue.get()
            ts = int(time.time())

            self.decodeTemperature(msg, ts)
            self.decodeHumidity(msg, ts)
            self.queue.task_done()


    def decodeTemperature(self, msg, ts):
        lsb3 = msg[12:16]
        raw = (struct.unpack('>h', bytes.fromhex(lsb3[2:]+lsb3[0:2]))[0]&0xFFFC)>>2
        val = (-40.0 + raw / 100.0)

        self.temperatureLock.acquire()
        self.temperature = val
        self.temperatureTS = ts
        self.temperatureLock.release()


    def decodeHumidity(self, msg, ts):
        msb3 = msg[10:14]
        raw = ((struct.unpack('>h', bytes.fromhex(msb3[2:]+msb3[0:2]))[0]&0x03FF))
        val = raw / 10.0

        self.humidityLock.acquire()
        self.humidity = val
        self.humidityTS = ts
        self.humidityLock.release()


    def getTemperature(self):
        self.temperatureLock.acquire()
        ts = self.temperatureTS
        val = self.temperature
        self.temperatureLock.release()

        if ts == 0:
            return jsonify("{}")

        tdict = dict(temperature = val, timestamp = ts)

        return jsonify(tdict)


    def getHumidity(self):
        self.humidityLock.acquire()
        ts = self.humidityTS
        val = self.humidity
        self.humidityLock.release()

        if ts == 0:
            return jsonify("{}")

        tdict = dict(humidity = val, timestamp = ts)

        return jsonify(tdict)


    def routes(self):
        routes=dict()
        routes["/devices/ble/" + self.name + "/temperature"] = dict(GET=self.getTemperature)
        routes["/devices/ble/" + self.name + "/humidity"] = dict(GET=self.getHumidity)
        return routes
