import logging
import threading
import os, pty
from serial import Serial


class LampDevice():
    def __init__(self, name, conf) -> None:
        self.name = name
        master, slave = pty.openpty() 

        # Translate the slave fd to a filename
        # and add it to the conf so the device handler can use it
        s_name = os.ttyname(slave) 
        conf[name]["port"] = s_name

        thread = threading.Thread(target=self.run, args=[master]).start()


    def run(self, readPort):
        status = 0
        logging.info("%s: started with status 0", self.name)

        while True:
            line = os.read(readPort, 8)
            print(self.name, ": ", line)

            if (line == b"GET\n"):
                print(self.name, ": ", status)
                msg = str(status) + "\n"
                os.write(readPort, msg.encode())
            elif (line == b"SET 1\n"):
                status = 1
            elif (line == b"SET 0\n"):
                status = 0
