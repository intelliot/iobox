import logging
import threading
import time
import smbus

LM75_ADDRESS          = 0x48
LM75_TEMP_REGISTER        = 0
LM75_CONF_SHUTDOWN    = 0
LM75_CONF_OS_COMP_INT = 1
LM75_CONF_OS_POL      = 2
LM75_CONF_OS_F_QUE    = 3

class I2CScanner():
    def __init__(self) -> None:
        self.registeredLock = threading.Lock()
        self.registered = dict()
        self.register_done = False
        self.started = []


    # executes on its own thread during initialization
    def register(self, register_queue):
        while True:
            try:
                # newDict holds:
                #
                # key -> device specific unique identifier
                # val -> device specific context, read/write queues for specific port
                newDict = register_queue.get(timeout=1)
                key = newDict["key"]
                val = newDict["val"]

                self.registeredLock.acquire()
                if self.register_done == False:
                    self.registered[key] = val
                self.registeredLock.release()

                register_queue.task_done()
            except:
                self.registeredLock.acquire()
                if self.register_done == True:
                    self.registeredLock.release()
                    logging.info("I2C devices registration completed")
                    return
                self.registeredLock.release()
      

    def start(self):
        self.registeredLock.acquire()
        self.register_done = True
        self.registeredLock.release()

        if len(self.registered) > 0:
            self.txrx()


    def txrx(self):
        for name, ctx in self.registered.items():
            if name in self.started:
                continue

            bus = smbus.SMBus(int(ctx["bus"]))
            address = int(ctx["address"], base = 16)
            reg = int(ctx["register"])
            readq = ctx["rwq"][0]
            tname = "I2C Handler " + name
        
            threading.Thread(target=self.rx, args=(bus, address, reg, readq), daemon=True, name=tname+" rx").start()

            self.started.append(name)


    # rx
    def rx(self, bus, address, register, readq):
        while True:
            time.sleep(2)
            raw = bus.read_word_data(address, register) & 0xFFFF

            #logging.info("i2c: rx reading message: %s", raw)
            readq.put(raw)
