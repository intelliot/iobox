import logging
import threading
import serial


class SerialScanner():
    def __init__(self) -> None:
        self.registeredLock = threading.Lock()
        self.registered = dict()
        self.register_done = False
        self.started = []


    # executes on its own thread during initialization
    def register(self, register_queue):
        while True:
            try:
                # newDict holds:
                #
                # key -> device specific unique identifier
                # val -> device specific context, read/write queues for specific port
                newDict = register_queue.get(timeout=1)
                key = newDict["key"]
                val = newDict["val"]

                self.registeredLock.acquire()
                if self.register_done == False:
                    self.registered[key] = val
                self.registeredLock.release()

                register_queue.task_done()
            except:
                self.registeredLock.acquire()
                if self.register_done == True:
                    self.registeredLock.release()
                    logging.info("Serial devices registration completed")
                    return
                self.registeredLock.release()
      

    def start(self):
        self.registeredLock.acquire()
        self.register_done = True
        self.registeredLock.release()

        if len(self.registered) > 0:
            self.txrx()


    def txrx(self):
        for port, rwq in self.registered.items():
            if port in self.started:
                continue

            readq = rwq[0]
            writeq = rwq[1]
            tname = "Serial Handler " + port
        
            threading.Thread(target=self.tx, args=(port, writeq), daemon=True, name=tname+" tx").start()
            threading.Thread(target=self.rx, args=(port, readq), daemon=True, name=tname+" rx").start()

            self.started.append(port)


    # tx
    def tx(self, port, writeq):
        while True:
            # block until get message from queue
            msg = writeq.get()
            logging.info("serial: tx sending (port=%s) message: %s", port, msg)

            with serial.serial_for_url(port) as transmitter:
                # adding '\n' for readline calls to succeed
                transmitter.write((str(msg)+"\n").encode())

            writeq.task_done()


    # rx
    def rx(self, port, readq):
        while True:
            with serial.serial_for_url(port) as receiver:
                # block until read
                line = receiver.readline()

            logging.info("serial: rx reading message: %s", line)
            readq.put(line)
