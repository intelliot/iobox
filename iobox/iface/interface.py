import logging
import threading
import queue
from . import iface_ble as ifaceBle
from . import iface_serial as ifaceSerial
from . import iface_i2c as ifaceI2C

SUPPORTED = ["iface_ble", "iface_serial", "iface_i2c"]


class Handler():
    def __init__(self, conf) -> None:
        self.conf = conf
        self.known_modules = dict(ble = False, serial=False, i2c = False)

        # Handlers registration queue. New queues are added
        # as needed 
        self.register_queue = dict()

        # Create instances for known interface handlers
        self.bleScanner = ifaceBle.BleScanner()
        self.serialScanner = ifaceSerial.SerialScanner()
        self.i2cScanner = ifaceI2C.I2CScanner()

        for section in conf:
            for option in conf[section]:
                if option == "module":
                    module = conf[section][option]

                    try:
                        state = self.known_modules[module]
                        if state == False:
                            self.startModule(module)
                    except:
                        logging.warning("Skip unknown module: ", module)


    def startModule(self, module):
        if module == "ble" and self.known_modules["ble"] == False:
            self.startBle()
        elif module == "serial" and self.known_modules["serial"] == False:
            self.startSerial()
        elif module == "i2c" and self.known_modules["i2c"] == False:
            self.startI2C()


    def startBle(self):
        self.register_queue["ble"] = queue.Queue(maxsize=10)
        
        # Start registration and scan threads
        logging.info("Starting BLE interface handler")
        threading.Thread(target=self.bleScanner.register, args={self.register_queue["ble"]}, daemon=True, name="BLE Scanner Thread").start()

        # Set module state to running
        self.known_modules["ble"] = True


    def startSerial(self):
        self.register_queue["serial"] = queue.Queue(maxsize=10)

        # Start registration and serial threads
        logging.info("Starting Serial interface handler")
        threading.Thread(target=self.serialScanner.register, args={self.register_queue["serial"]}, daemon=True,name="Serial Scanner Thread").start()

        # Set module state to running
        self.known_modules["serial"] = True


    def startI2C(self):
        self.register_queue["i2c"] = queue.Queue(maxsize=10)

        # Start registration and serial threads
        logging.info("Starting I2C interface handler")
        threading.Thread(target=self.i2cScanner.register, args={self.register_queue["i2c"]}, daemon=True,name="I2C Scanner Thread").start()

        # Set module state to running
        self.known_modules["i2c"] = True


    def registerModule(self, module, devKey, devCtx):
        try:
            self.register_queue[module].put(dict(key = devKey, val = devCtx))
            logging.info("interface: registering '%s' at module '%s' with ctx '%s'", devKey, module, devCtx)
        except:
            logging.info("interface: skip registering '%s' at unknown module '%s'", devKey, module)


    def start(self):
        self.bleScanner.start()
        self.serialScanner.start()
        self.i2cScanner.start()
