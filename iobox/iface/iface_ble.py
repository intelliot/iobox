import logging
import threading
import time
from bluepy.btle import Scanner, DefaultDelegate


class BleScanner():
    def __init__(self) -> None:
        self.registeredLock = threading.Lock()
        self.registered = dict()
        self.register_done = False


    # executes on its own thread during initialization
    def register(self, register_queue):
        while True:
            try:
                # newDict holds:
                #
                # key -> device specific unique identifier
                # val -> device specific context, ble message receive queue
                newDict = register_queue.get(timeout=1)
                key = newDict["key"]
                val = newDict["val"]

                self.registeredLock.acquire()
                if self.register_done == False:
                    self.registered[key] = val
                self.registeredLock.release()

                register_queue.task_done()
            except:
                self.registeredLock.acquire()
                if self.register_done == True:
                    self.registeredLock.release()
                    logging.info("BLE devices registration completed")
                    return
                self.registeredLock.release()
        

    def start(self):
        self.registeredLock.acquire()
        self.register_done = True
        self.registeredLock.release()

        if len(self.registered) > 0:
            threading.Thread(target=self.scan, daemon=True).start()


    # executes on its own thread
    def scan(self):
        while True:
            scanner = Scanner().withDelegate(ScanDelegate(self.registered))
            try:
                scanner.scan(1)
            except:
                logging.warn("BLE scanner needs elevated rights!")

            time.sleep(0.1)


class ScanDelegate(DefaultDelegate):
    def __init__(self, registered) -> None:
        DefaultDelegate.__init__(self)
        self.queues = registered


    def handleDiscovery(self, dev, isNewDev, isNewData):
        try:
            mac = str(dev.addr)
            self.queues[mac].put(dev.getScanData()[0][2])
        except:
            pass
