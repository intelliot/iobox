all:
	iobox /etc/iobox/config.ini

image:
	docker build -t tsi-iobox:1.0 .

install:
	pip install --upgrade .
	install -D -t /etc/iobox config.ini
	install -D -t /etc/systemd/system iobox.service
	systemctl daemon-reload

uninstall:
	pip uninstall iobox
	rm -rf /etc/iobox
	rm -f /etc/systemd/system/iobox.service
	systemctl daemon-reload

clean:
	find iobox/ -type d -name "__pycache__" -print0 | xargs -I {} -0 rm -rf {}
	rm -rf iobox.egg-info build

.PHONY: all image install uninstall clean
