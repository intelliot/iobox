# Interoperability Box

The Interoperability Box is a software component that requires a Linux-capable
device to be installed on with proper communication interfaces for each device
type it is going to support. We use a Raspberry Pi as reference device.

The Interoperability Box is comprised of the following parts:

- interface handlers
- device handlers
- and the REST API

Interface handlers are responsible to transfer messages between an interface,
e.g. BLE, and the device handlers. Interface handlers can have multiple types
of devices registered to them and multiple devices per device type.

The device handler is specific to a device type, e.g. MiroCard, and it provides
the glue between the constrained device and the REST API. It has knowledge for
the specific device type payloads and means of communication as well as
provision of the REST API routes. For each device used, a new instance of the
appropriate device handler is created.

Finally, the REST API is based on Flask and it is populated by the routes that
the device handlers provide.

The Interoperability Box is part of `Use Case 3 - Manufacturing` of the
[IntellIoT project](https://intelliot.eu/).



## ToDo

- Get a thing description, e.g. iobox.td, from HSG
- Add iobox.td registration to hypermas
- Add functionality for Open Call 2 winners



## Quick Start

The Interoperability Box uses Python 3.9 or later. It has the following
dependencies:

- argparse
- bluepy
- configparser
- flask
- pyserial

bluepy needs glib development files present and a compiler. For ubuntu that
would be:

    sudo apt-get install build-essential libglib2.0-dev

Install:

    # Need sudo to install system-wide at /usr/local/bin/iobox
    sudo make install

Uninstall:

    # Need sudo as a consequence of installing with sudo
    sudo make uninstall
    sudo make clean

Usage:

    # To run in console
    # Here sudo is needed to get the BLE scan rights
    sudo make
    
    # OR to run as a service
    sudo systemctl enable iobox
    sudo systemctl start iobox

This will start the iobox daemon using the provided configuration file.
With this configuration you will get one MiroCard and two FakeLamp devices.
The MiroCard should be located near the Raspberry Pi. The FakeLamp devices
are pseudo-devices to demonstrate serial communication.

Below you will find the available endpoints using the provided configuration.
You can also import the `iobox.postman.json` at [Postman](https://www.postman.com/).

Lastly, you can also build a docker image, if needed:

    make image

And then run it with your configuration:

    docker run -d -p 5000:5000 -v ./your_config.ini:/app/config.ini tsi-iobox:1.0



### Healthcheck Endpoint

Returns an empty object.

    GET /

Example request:

    curl -i http://127.0.0.1:5000/

Example response:

    {}



### Registered devices by type

Returns the number of registered devices per device type.

    GET /devices

Example request:


    curl -i http://127.0.0.1:5000/devices


Example response:

    {
        "ble": 1,
        "serial": 2
    }



### Registered BLE devices

Returns the configuration of each registered BLE device.

    GET /devices/ble

Example:

    curl -i http://127.0.0.1:5000/devices/ble

Example response:

    {
        "mirocard_testbed": {
            "mac": "60:77:71:57:17:c9",
            "module": "ble",
            "name": "testbed",
            "type": "mirocard"
        }
    }



### Registered Serial devices

Returns the configuration of each registered Serial device.

    GET /devices/serial

Example:

    curl -i http://127.0.0.1:5000/devices/serial

Example response:

    {
        "fakelamp1": {
            "module": "serial",
            "name": "fakelamp1",
            "port": "/dev/pts/10",
            "type": "fakelamp"
        }, 
        "fakelamp2": {
            "module": "serial",
            "name": "fakelamp2",
            "port": "/dev/pts/11",
            "type": "fakelamp"
        }
    }



### Get MiroCard temperature

Returns the temperature resource of this specific device.

    GET /devices/ble/mirocard_testbed/temperature

Example:

    curl -i http://127.0.0.1:5000/devices/ble/mirocard_testbed/temperature

Example response:

    {
        "temperature": 21.58,
        "timestamp": 1678460287
    }



### Get MiroCard humidity

Returns the humidity resource of this specific device.

    GET /devices/ble/mirocard_testbed/humidity

Example:

    curl -i http://127.0.0.1:5000/devices/ble/mirocard_testbed/humidity

Example response:

    {
        "humidity": 38.7,
        "timestamp": 1678460297
    }



### Get FakeLamp status

Returns the FakeLamp status. There are two FakeLamp instances to choose. 'fakelamp1' and 'fakelamp2'.

    GET /devices/serial/fakelamp1/status 

Example:

    curl -i http://127.0.0.1:5000/devices/serial/fakelamp1/status

Example response:

    {
        "name": "fakelamp1",
        "status": 0,
        "timestamp": 0
    }



### Set FakeLamp status

Sets the FakeLamp status and returns the result. There are two FakeLamp instances to choose. 'fakelamp1' and 'fakelamp2'.

    PUT /devices/serial/fakelamp2/status

Parameters:

The requested status is passed inside the request body in json format. Status can either be 0 or 1:

    {
        "status": 1
    }

Example:

    curl -i -X PUT -H 'Content-Type: application/json' -d '{"status":0}' http://127.0.0.1:5000/devices/serial/fakelamp1/status

Example response:

    {
        "name": "fakelamp1",
        "status": 0,
        "timestamp": 1678460883
    }



## Developer Notes

Start under venv:

    python -m venv venv
    source ./venv/bin/activate

Install:

    pip install --upgrade .

Uninstall:

    pip uninstall iobox
    make clean

Run:

    iobox config.ini

Exit venv:

    deactivate

Clean venv:

    rm -rf venv

### Adding a new Interface handler

In order to support a new interface you have to create under `iobox/iface` a
new file, for example:

    iface_lora.py

You can use the existing interfaces as templates. You will also need to add
the appropriate glue code of the new interface at `interface.py`.

### Adding a new Device handler

To add a new device handler you have to create under `iobox/device` a new file
, for example:

    dev_constrained.py

You can use the existing device handlers as templates. You will also need to
add the appropriate glue code of the new devicel handler at `device.py`.
